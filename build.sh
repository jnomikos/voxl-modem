#!/bin/bash

set -e

./clean.sh

if [ $1 == "apq8096" ]
then
    cd src/modem
    ./build.sh
    cd ..

    cd configband
    make
    cd ../..
fi

cd src/quectel-CM
make
cd ../..

set +e
