# File: modallink_relink .py
# Company: Modal-AI, Inc.
# Author: Rich Martin (rich@modalai.com)
# Description: Monitors Modal-Link connection. If the connection is no longer active,
# the interface is removed and the modem connection is reinitialized. 

import os
from time import sleep
import argparse

#loop until connection is not active
#tear down interface
#restart modem services
#exit

def check_link(gateway):
    #test
    active = False
    response = os.system("ping -c 1 " + gateway +  " > /dev/null 2>&1")
    
    if response == 0:
        active = True
    else:
        active = False

    return active


def main():

    gateway_default = "172.16.0.1"
    modem_default = "v2"
    rest_secs = 2
    iface = ''
    service = 'voxl-modem'

    parser = argparse.ArgumentParser(description='ModalLink Re-Link')
    parser.add_argument('-g','--gw', dest='gateway', action='store',
                        default=gateway_default,
                        help = "ModalLink gateway IP address.")
    parser.add_argument('-m','--modem', dest='modem', action='store',
                        default=modem_default,
                        help = "Modem name, supported modems: v1, v2 ")
    args = parser.parse_args()
    
    gateway = str(args.gateway).lower()
    modem = str(args.modem).lower()


    if modem == "v2":
        iface = "wwan0"
    elif modem == "v1":
        iface = "rmnet_usb0"
    else:
        print "Modem ", modem, " not supported."
        return

    while check_link(gateway):
        #debug
        print "Connection active . . ."
        sleep(rest_secs)
    
    print "Connection inactive. Restarting modem services . . . "

    #remove modem interface
    os.system("ip link set " + iface + " down")

    #restart service
    os.system("systemctl restart " + service)

if __name__ == "__main__":
    main()
