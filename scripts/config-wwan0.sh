#!/bin/bash
#Configure Sierra Modem wwwan0 interface after it is brought up. 

#configure wwan0 LTE interface netmask
echo -e "\nConfiguring wwan0 LTE interface netmask..."
ifconfig wwan0 `ifconfig wwan0 | awk '/t addr:/{gsub(/.*:/,"",$2);print$2}'` netmask 255.255.255.0

#set default gateway to femtocell
echo -e "\nSetting default gateway..."
ip route add default via 172.16.0.1 dev wwan0

#set wwan0 MTU size
echo -e "\nSetting wwan0 MTU size..."
ifconfig wwan0 mtu 1500 
