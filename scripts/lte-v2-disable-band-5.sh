# echo 'AT!ENTERCND="A710"' > /dev/ttyUSB2
# echo 'at!impref="Generic"' > /dev/ttyUSB2
# Power cycle device

PORT="ttyUSB2"

# Loop until correct /dev/tty device is available
enable-sierra.sh &> /dev/null
	
rc=1
echo -e "\nWaiting for "${PORT}"..."
while [ $rc -ne 0 ]; do
    ls /dev | grep "$PORT"
    rc=$?
    sleep 1
done

echo -e ${PORT}" initialized"

echo 'AT!ENTERCND="A710"' > /dev/ttyUSB2
echo 'at!band=0a,"nb5",0000000002800000,000000000001380A,0000000000000002,0000000000000000' > /dev/ttyUSB2
echo 'AT!BAND=0a' > /dev/ttyUSB2
echo -e "\nFinished disabling band 5."
