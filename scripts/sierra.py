#!/usr/bin/python
################################################################################
# Copyright 2019, 2020 ModalAI Inc.
#
# Redistribution and use in source and binary forms, with or without
# modification, are permitted provided that the following conditions are met:
#
# 1. Redistributions of source code must retain the above copyright notice,
#    this list of conditions and the following disclaimer.
#
# 2. Redistributions in binary form must reproduce the above copyright notice,
#    this list of conditions and the following disclaimer in the documentation
#    and/or other materials provided with the distribution.
#
# 3. Neither the name of the copyright holder nor the names of its contributors
#    may be used to endorse or promote products derived from this software
#    without specific prior written permission.
#
# 4. The Software is used solely in conjunction with devices provided by
#    ModalAI Inc.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
# AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
# IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
# ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE
# LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
# CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
# SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
# INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
# CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
# ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
################################################################################

import os
import sys
import time
import subprocess
import signal

import getopt

#==============================================================================
# Constants
#==============================================================================

VERSION = "0.8.0"

FNULL = open(os.devnull, 'w')  # route unwanted console ouput

#------------------------------------------------------------------------------
# GENERAL functions
#------------------------------------------------------------------------------

## Determines the existence of a specific serial port
#   @param port
#       serial port name (i.e. /dev/ttyUSB0)
#   @returns
#       - true if it exists
#       - false otherwise
#
def check_for_serial_port(port):
    counter = 0
    print '    checking for serial port: %s' % port
    while not os.path.exists(port):
        time.sleep(2)
        if counter == 20:
            return False
        counter += 1
    return True


##
#  @param modem_on_usb
#       - True modem is connected via USB
#       - False modem is connected using a feather board
#
def wait_for_diag_serial_port_to_activate( diag_serial_port, modem_on_usb = False ):
    print 'Waiting for the diag serial port(%s) to activate' % diag_serial_port

    # Hardcoded for WNC feather board only
    # The Sierra module uses USB2

    max_retry = 5

    while not check_for_serial_port(diag_serial_port):
        max_retry -= 1
        print '    ' + diag_serial_port + ' not found.'

        if modem_on_usb == False:
            print '    Power cycling feather'
            # feather_cycle_power( diag_serial_port )
        else:
            print '    Waiting for 2 seconds for modem to appear'
            time.sleep(2)

        if max_retry <= 0:
            print ('    Aborting, failed to detect: ' + diag_serial_port )
            sys.exit(1)


    print '    ' + diag_serial_port + ' is available'
    return True

##
#
def verify_qmi_dev( qmi_dev, exitOnFailure = True ):
    print 'Verifying existence of QMI port(%s)' % qmi_dev

    if not check_for_serial_port(qmi_dev):
        print '    ' + qmi_dev + ' does not exist'
        if exitOnFailure:
            sys.exit(-1)

    print '    ' + qmi_dev + ' is available'

## Some of the Sierra modems fail on the first few QMI attempts.
#  This will get us past that so that all future QMI attempts work.
#
def unstick_qmi( qmi_dev ):
    qmi_retry_count = 10
    qmi_response_timeout = 5
    success = False

    while (not success) and (qmi_retry_count > 0):
        print 'Unstick QMI countdown: ' + str(qmi_retry_count)
        task = subprocess.Popen(['uqmi', '-P', '-d', qmi_dev, '--get-msisdn'])
        while (not success) and (qmi_response_timeout > 0):
            if task.poll() is None:
                # print 'Task is still running'
                time.sleep(1.0)
                qmi_response_timeout -= 1;
            else:
                # print 'Task finished'
                success = True
        # print 'Trying to kill the process'
        try:
            task.kill()
        except OSError:
            pass # ignore
        task.wait()
        qmi_retry_count -= 1
        qmi_response_timeout = 4

    return success

## Checks that the modem has successfully registered with the cellular system.
#
def uqmi_check_registration( qmi_dev, retry_count = 12 ):

    attempts = 0
    print 'Checking for system registration'
    registered = False
    while not registered:
        attempts += 1
        print '   Attempt: ' + str(attempts)
        response = ''
        try:
            response = subprocess.check_output([ "uqmi", "-P", "-d", qmi_dev,
                                                "--get-serving-system"],
                                            stderr=FNULL)
            print response
        except subprocess.CalledProcessError:
            print '    Got exception when checking registration status'
            sys.exit(-1)

        if '"registration":' in response:
                print 'response: [' + response + ']'
                tokens = response.split('"')
                if len(tokens) > 4:
                    if tokens[1] == 'registration':
                        if tokens[3] == 'registered':
                            print '    Successfully registered with the network'
                            registered = True
                        else:
                            print tokens[1] + ": " + tokens[3]
        retry_count -= 1
        if retry_count == 0:
            print '    Giving up on registration'
            sys.exit(-1)
        if not registered:
            time.sleep(5)

## Starts network connectivity
#
def uqmi_start_network(qmi_dev):
    print 'Starting network'

    try:
        subprocess.call(["uqmi", "-d", qmi_dev,
                        "-k", "wds", "--start-network"],
                        stderr=FNULL)
    except subprocess.CalledProcessError:
        print '    Got exception when starting network'
        sys.exit(-1)

    print '    Checking connection status'
    try:
        response = subprocess.check_output(["uqmi", "-d", qmi_dev,
                                            "-k", "wds", "--get-data-status"],
                                        stderr=FNULL)
    except subprocess.CalledProcessError:
        print '        Could not check connection status'
        sys.exit(-1)
    if response.rstrip() != '"connected"':
        print response
        print '        Connection failed'
        sys.exit(-1)
    else:
        print '        Connection is up'

##  Uses udhcpc to issue a DHCP client request.
#
#   @param network_interface
#       Network interface (i.e. wwan0 or rmnet_usb0) to use.
#
def dhcp_request(network_interface):

    print 'Using the DHCP client to get an IP address'
    # dhcp_command = ['udhcpc', '-v', '-i', network_interface, '-s',
    #                 '/usr/local/qr-linux/udhcpc.sh']
    dhcp_command = ['udhcpc', '-q', '-f', '-i', network_interface]

    # TODO: Add a timeout for this. It can hang
    print subprocess.check_output(dhcp_command)

##  Change to LTE only. Disables other modes like WCDMA.
#
#   @param qmi_dev
#       i.e. /dev/qcqmi0
#
def uqmi_set_network_lte_only(qmi_dev):

    print 'Setting network mode to LTE only'

    try:
        status = subprocess.check_output(["uqmi", "-d", qmi_dev, \
                                          "--set-network-modes", "lte"], \
                                         stderr=FNULL)
    except subprocess.CalledProcessError:
        print '    Warning: got exception when setting network mode to LTE only'
        sys.exit(-1)

    print '    Network mode set to LTE only'

##  Configure the LTE bands to use
#   @param  port
#       Serial port to use for Qualcomm Diag commands.
#   @param value
#       The bitmask to use for band configuration.
#
def configure_lte_bands(port, value1, value2 = 0, value3 = 0):

    print 'Configuring LTE bands'

    try:
        status = subprocess.check_output(["configband", "write", port, \
                                          value1, value2, value3], \
                                         stderr=FNULL)
    except subprocess.CalledProcessError:
        print '    Warning: got exception when setting band configuration'
        sys.exit(-1)

    print '    LTE bands configured'

##
#  This pulls the Sierra module on the feather board out of reset.
#  This is harmless for external USB Sierra boards.
#
def pull_module_out_of_reset():
    print 'Configuring GPIO to pull modem out of reset'
    subprocess.call("echo 107 > /sys/class/gpio/export",
                    stderr=FNULL, shell=True)
    time.sleep(1)
    subprocess.call("echo out > /sys/class/gpio/gpio107/direction",
                    stderr=FNULL, shell=True)
    time.sleep(1)
    subprocess.call("echo 1 > /sys/class/gpio/gpio107/value",
                    stderr=FNULL, shell=True)
    time.sleep(1)

##
#  Prints usage info.
#
def print_help():
        print "Usage: python sierra.py [options]"
        print ""
        print "    v%s Copyright 2019, 2020 ModalAI Inc." % VERSION
        print ""
        print "    --disable_band_5             Disables band 5 for aircraft use"
#        print "    --band_3_only                Only enable band 3 (U.S. DoD use)"
        print ""

#--------------------------------------------------------------------
# Main
#--------------------------------------------------------------------

disable_band_5 = False

try:
    opts, args = getopt.getopt(sys.argv[1:], "h", ["disable_band_5"])

except getopt.GetoptError as err:
    print "Error: ", err
    print_help()
    sys.exit(-1)

# process command line arguments
#
for opt, arg in opts:

    opt_assignments = {}
    opt_params = arg.split(',')
    for config_param in opt_params:
        assignment =  config_param.split("=")
        if len(assignment) >= 2:
            opt_assignments[ assignment[0] ] = assignment[1]

    if opt == '-h':
        print_help()
        sys.exit(0)

    if opt in ("--disable_band_5"):
        disable_band_5 = True

pull_module_out_of_reset()
wait_for_diag_serial_port_to_activate('/dev/ttyUSB0', True)
verify_qmi_dev('/dev/cdc-wdm0')
if not unstick_qmi('/dev/cdc-wdm0'):
    print 'Failed to get QMI working'
    sys.exit(-1)

if disable_band_5:
    uqmi_set_network_lte_only('/dev/cdc-wdm0')
    # WP7610 bands 2, 4, 12, 13, 14, 17, and 66
    configure_lte_bands('/dev/ttyUSB0', '0x380a', '0x1', '0x2')

uqmi_check_registration('/dev/cdc-wdm0')
uqmi_start_network('/dev/cdc-wdm0')
os.system('echo Y > /sys/class/net/wwan0/qmi/raw_ip')
dhcp_request('wwan0')
