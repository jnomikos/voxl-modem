#!/bin/bash

# Enter passcode to enable extended commands
echo 'at!entercnd="A710"' > /dev/ttyUSB2

#
# FIRMWARE
#

# Query current firmware in use. Should be "Generic"
# echo 'at!impref?' > /dev/ttyUSB2

# Set prefered firmware version to "Generic"
# This is persistent on 7607
echo 'at!impref="Generic"' > /dev/ttyUSB2

#
# RAT
#

# Query current Radio Access Technology (RAT) setting
# echo 'at!selrat?' > /dev/ttyUSB2

# Query available Radio Access Technology (RAT) settings
# echo 'at!selrat=?' > /dev/ttyUSB2

# Set Radio Access Technology (RAT) to LTE only.
# This is persistent on 7607
echo 'at!selrat=06' > /dev/ttyUSB2

#
# BANDS
#

# Query band selection table
# echo 'at!band=?' > /dev/ttyUSB2

# Query current table entry selection
# echo 'at!band?' > /dev/ttyUSB2

# Create new table entry that only allows band 3 in LTE
# This is persistent on 7607
echo 'at!band=0a,"ob3",0000000000000000,0000000000000004,0000000000000000,0000000000000000' > /dev/ttyUSB2

# Select the new table entry
# This is persistent on 7607
echo 'at!band=0a' > /dev/ttyUSB2
