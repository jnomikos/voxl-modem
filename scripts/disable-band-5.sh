#!/bin/bash

# Use AT commands to disable band 5 for aircraft use
echo 'at!entercnd="A710"' > /dev/ttyUSB2
echo 'at!band=0a,"nb5",0000000002800000,000000000000380A,0000000000000000,0000000000000000' > /dev/ttyUSB2
echo 'at!band=0a' > /dev/ttyUSB2
