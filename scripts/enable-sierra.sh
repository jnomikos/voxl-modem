#!/bin/bash

# Pull the Sierra module out of reset with proper GPIO
echo 107 > /sys/class/gpio/export
echo out > /sys/class/gpio/gpio107/direction
echo 1 > /sys/class/gpio/gpio107/value
