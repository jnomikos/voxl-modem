# voxl-modem

This repository contains the required utilities for the following modems:
- ModalAI v1 LTE (WNC CSS Modules, apq8096 only)
- ModalAI v2 LTE (Sierra Modules)
- Microhard Add-on board
- Quectel Modem (5G)
- Doodle Labs (USB)

See usage instructions and more on the [Modems](https://docs.modalai.com/modems/) page of the docs.

## Build Instructions

1) prerequisite: docker image voxl-cross:V1.5 or greater

Follow the instructions here:

https://gitlab.com/voxl-public/voxl-docker


2) Launch Docker and make sure this project directory is mounted inside the Docker.

```bash
~/git/voxl-modem$ voxl-docker -i voxl-cross

voxl-cross:~$ ls
CHANGELOG         build.sh               install_on_voxl.sh  src
CMakeLists.txt    clean.sh               ipk                 utils
LICENSE           config                 make_package.sh
README.md         include                misc
bash_completions  install_build_deps.sh  service
```

3) Compile inside docker.

```bash
voxl-cross:~$ ./build.sh
```

4) Make an ipk/deb package inside docker.

```bash
voxl-cross:~$ ./make_package.sh

Package Name:  voxl-modem
version Number:  x.x.x
...

Done making voxl-modem_x.x.x.ipk
```

This will make a new `voxl-modem_x.x.x.ipk` file in your working directory. The name and version number came from the `ipk/control/control` file. If you are updating the package version, edit it there.

## Deploy to VOXL

You can now push the ipk package on target and install using the following:

```bash
~/git/voxl-modem$ ./deploy_to_voxl.sh
pushing voxl-modem_x.x.x.ipk to target
searching for ADB device
adb device found
voxl-modem_x.x.x.ipk: 1 file pushed. 2.1 MB/s (51392 bytes in 0.023s)
Removing package voxl-modem from root...
Installing voxl-modem (x.x.x) on root.
Configuring voxl-modem.

Done installing voxl-modem
```
