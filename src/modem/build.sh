#!/bin/bash

set -e

# Build and install Lua 5.1
# This is a dependency for libubox
cd lua-5.1.5
sudo make linux install
cd ..

# Build and install json-c
# This is a dependency for libubox
if [ ! -d json-c ]; then
    git clone https://github.com/json-c/json-c.git
fi
cd json-c
git checkout af8dd4a307e7b837f9fa2959549548ace4afe08b
if [ ! -f configure ]; then
    sh autogen.sh
fi
./configure --prefix=/usr
make
sudo make install
cd ..

# Build and install libubox
# This is a dependency for uqmi
cd libubox
cmake .
make
sudo make install
sudo cp lib*.so /usr/lib
cd ..

# Build uqmi
cd uqmi
cp msm_rmnet.h /usr/include/linux/
cmake .
make
cd ..

set +e
